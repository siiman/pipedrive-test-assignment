FROM public.ecr.aws/lambda/python:3.8

COPY pipedrive.py ${LAMBDA_TASK_ROOT}
COPY gists.html.j2 ${LAMBDA_TASK_ROOT}
COPY requirements.txt  .

RUN  pip3 install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

CMD [ "pipedrive.handler" ]