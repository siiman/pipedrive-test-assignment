terraform {
  backend "http" {
  }
}

provider "aws" {
  region = var.region
}

variable "region" {
    type = string  
}
variable "bucket_name" {
    type = string
}
variable "repo" {
    type = string
}
variable "github_token" {
  type = string
}

data "aws_iam_role" "pipedrive_lambda" {
  name = "pipedrive_lambda"
}

resource "aws_lambda_function" "pipedrive" {
	package_type = "Image"
  	image_uri = "${var.repo}:latest"
  	function_name = "pipedrive"
  	role = data.aws_iam_role.pipedrive_lambda.arn
  	environment {
    	variables = {
      		BUCKET_NAME = var.bucket_name
      		GH_TOKEN = var.github_token
    	}
  	}
}

resource "aws_lambda_permission" "eventbridge_perms" {
	action = "lambda:InvokeFunction"
	function_name = aws_lambda_function.pipedrive.function_name
	principal = "events.amazonaws.com"
	source_arn = aws_cloudwatch_event_rule.cron.arn
}

resource "aws_cloudwatch_event_rule" "cron" {
	name = "lambda_cron"
	schedule_expression = "rate(3 hours)" 
}

resource "aws_cloudwatch_event_target" "pipedrive_target" {
	rule = aws_cloudwatch_event_rule.cron.name
	arn = aws_lambda_function.pipedrive.arn
}