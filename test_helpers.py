from github import Github, UnknownObjectException
from botocore.response import StreamingBody
import io

class NoUserResponse:
    def get_gists(self, since):
        raise UnknownObjectException(404, "test", {"test": "test"})

class UserExistsResponse:
    def get_gists(self, since):
        return "Was I added?"

class mockPagiantedList:
    def __init__(self, gists):
        self.gists = gists
    
    def __iter__(self):
        yield from self.gists

class mockNamedUser:
    def __init__(self, login):
        self.login = login

    def login(self):
        return self.login.value

class mockGist:
    def __init__(self, html_url, id, created_at, owner):
        self.html_url = html_url
        self.id = id
        self.created_at = created_at
        self.owner = mockNamedUser(owner)

def make_users_response(response):
    stream = StreamingBody(io.BytesIO(response),len(response)) 
    return {"Body": stream}
