from github import Github, UnknownObjectException
import boto3
from datetime import datetime
from jinja2 import Environment, FileSystemLoader
import logging
import os
import json
import requests

GISTS_S3_KEY = "gists.html"
USERS_S3_KEY = "users.json"
GISTS_TEMPLATE = "gists.html.j2"

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

def get_last_scrape_time(s3_client):
    try:
        return s3_client.get_object(Bucket=os.environ["BUCKET_NAME"], Key=GISTS_S3_KEY)["LastModified"]
    except s3_client.exceptions.NoSuchKey:
        logging.warning("Bucket object does not exist yet, scrape time is set to now.")
        return datetime.utcnow()

def get_gists(conf, last_scrape_time):
    gists = []
    g = Github(os.environ["GH_TOKEN"])
    for u in conf["users"]:
        try:
            users_gists = g.get_user(login=u).get_gists(since=last_scrape_time)
            if len(list(users_gists)) > 0:
                gists.append(users_gists)
                #create_pipedrive_action_for_gist(subject=f"New gist(s) for user {u}")
        except UnknownObjectException:
            logging.error("User not found.")
    return gists

def render_html(last_scrape_time, gists):
    environment = Environment(loader=FileSystemLoader("./"))
    html_template = environment.get_template(name=GISTS_TEMPLATE)
    return html_template.render(last_scrape_time=last_scrape_time, gists=gists)

def upload_to_s3(body, s3_client):
    s3_client.put_object(Bucket=os.environ["BUCKET_NAME"], Body=body, Key=GISTS_S3_KEY, ContentType='text/html')

def init_s3_connection():
    return boto3.client("s3", endpoint_url=os.environ.get("S3_URL"))

def load_config_about_users(s3_client):
    users_conf = json.loads(s3_client.get_object(Bucket=os.environ["BUCKET_NAME"], Key=USERS_S3_KEY)["Body"].read().decode("utf-8"))
    if len(users_conf["users"]) < 1:
        raise ValueError("User list is empty.")
    return users_conf

def create_pipedrive_action_for_gist(subject):
    # the assignment did not go into detail about creating a pipedrive action, 
    # so just made it pretty basic...
    requests.post(url=os.environ["PIPEDRIVE_URL"], data={"subject": subject, "type": "gist"})

def handler(event, context):
    s3_client = init_s3_connection()
    users = load_config_about_users(s3_client)
    last_scrape_time = get_last_scrape_time(s3_client)
    gists = get_gists(users, last_scrape_time)
    gists_html = render_html(last_scrape_time, gists)
    upload_to_s3(gists_html, s3_client)