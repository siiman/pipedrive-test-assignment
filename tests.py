import json
from botocore.stub import Stubber
import boto3
from datetime import datetime
import pipedrive
import pytest
from github import Github
import test_helpers as helpers

client = boto3.client("s3")
stubber = Stubber(client)

### AWS related tests

## load config
def test_loading_users_config_with_items():
    r = json.dumps({"users": ["test"]}).encode()
    stubber.add_response('get_object', helpers.make_users_response(r))
    with stubber:
        result = pipedrive.load_config_about_users(client)
    assert len(result["users"]) == 1

def test_loading_empty_users_config():
    r = json.dumps({"users": []}).encode()
    stubber.add_response('get_object', helpers.make_users_response(r))
    with pytest.raises(ValueError):
        with stubber:
            pipedrive.load_config_about_users(client)

## get_last_scrape_time
def test_last_scrape_time_when_object_does_exist():
    r = {"LastModified": datetime(2022,8,4,20,50)}
    stubber.add_response('get_object', r)
    with stubber:
        result = pipedrive.get_last_scrape_time(client)
    assert result == datetime(2022,8,4,20,50)

def test_last_scrape_time_when_object_does_not_exist():
    stubber.add_client_error('get_object', service_error_code='NoSuchKey')
    stubber.activate()
    with stubber:
        result = pipedrive.get_last_scrape_time(client)
    assert result.strftime("%Y-%m-%d %H:%M:%S") == datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

### Github related tests
## get_gists
def test_that_empty_array_is_return_when_user_does_not_exist(monkeypatch):
    def mock_get_user(self, login):
        return helpers.NoUserResponse()
    
    monkeypatch.setattr(Github, "get_user", mock_get_user)
    users = {"users":["test"]}
    result = pipedrive.get_gists(users, datetime.utcnow())
    assert result == []


def test_when_user_exists_then_item_is_added_to_result_array(monkeypatch):
    def mock_get_user(self, login):
        return helpers.UserExistsResponse()
    
    monkeypatch.setattr(Github, "get_user", mock_get_user)
    users = {"users":["test"]}
    result = pipedrive.get_gists(users, datetime.utcnow())
    assert result == ["Was I added?"]

## Misc
## render_html
two_gists = [
    helpers.mockGist("example.com", "123", datetime(2022,8,4,20,50), "test1"),
    helpers.mockGist("example.com", "321", datetime(2022,8,4,20,55), "test2")
]
def test_rendering_with_two_new_gists():
    pl = [helpers.mockPagiantedList(gists=two_gists)]
    exptected_result = """<!doctype html>
<html lang="en">
<head>
    <title>Scraping public Github gists</title>
</head>
<body>
Since 2022-08-04 20:40:00 these Gists were added:
    <ul>
        <li><a href="example.com">123</a> by test1 at 2022-08-04 20:50:00</li>
        <li><a href="example.com">321</a> by test2 at 2022-08-04 20:55:00</li>
    </ul>
</body>"""
    assert (pipedrive.render_html(last_scrape_time=datetime(2022,8,4,20,40), gists=pl)) == exptected_result

def test_rendering_with_no_new_gists():
    pl = []
    expected_result = """<!doctype html>
<html lang="en">
<head>
    <title>Scraping public Github gists</title>
</head>
<body>
No new gists since 2022-08-04 20:40:00!
</body>"""
    assert (pipedrive.render_html(last_scrape_time=datetime(2022,8,4,20,40), gists=pl)) == expected_result
